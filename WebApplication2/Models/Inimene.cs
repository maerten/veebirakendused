﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations; //et saaks Key öelda

namespace WebApplication2.Models
{
    public class Inimene
    {
        static int algus = 0;
        [Key]public int Nr { get; set; }
        public string Nimi { get; set; }
        public int Vanus { get; set; }
    }
}